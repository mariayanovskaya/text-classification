
# coding: utf-8

# ## Text Classification (business/personal communication)  **TEST TASK**
# By Maria Yanovskaya

#this contains all modules used throughout this notebook except for sklearn, which was imported as required
import pandas as pd
import glob
import string
import re
import nltk
import numpy as np
from datetime import datetime as dt


stopwords = nltk.corpus.stopwords.words('english')
ps = nltk.PorterStemmer()
wn = nltk.WordNetLemmatizer()



#empty_list_b =[]
#for i in (glob.glob('./business/*')):
#    rawfiles_opened = open(i, encoding='windows-1252').read()
#    empty_list_b.append(rawfiles_opened)
#
#empty_list_p =[]
#for i in (glob.glob('./personal/*')):
#    rawfiles_openedp = open(i, encoding='windows-1252').read()
#    empty_list_p.append(rawfiles_openedp)
#
#emails_df = pd.DataFrame(data = [empty_list_b,
#                                 np.ones(len(glob.glob('./business/*')), dtype = int).tolist()])
#emails_p = pd.DataFrame(data = [empty_list_p,
#                                 np.zeros(len(glob.glob('./personal/*')), dtype = int).tolist()]).T
#emails_df = emails_df.T
#emails_df = pd.concat([emails_df,emails_p], axis =0)
#emails_df.columns = ['text', 'category']
#
#from sklearn.utils import shuffle
#emails_df = shuffle(emails_df).reset_index(drop=True)


#functions for feature extraction
def char_calc(text):
    '''calculates number of all characters, excluding spaces, tabs and new lines'''
    no = len(text) - text.count(" ") - text.count("\n") - text.count("\t")
    return no

def punct_count(text):
    '''calculates number of punctuation'''
    pun_no = sum([1 for char in text if char in string.punctuation])
    return pun_no

def digits_count(text):
    '''calculates number of digits'''
    dig_no = sum([1 for char in text if char in string.digits])
    return dig_no

def upcase_count(text):
    '''calculates number of uppercase letters'''
    upc_no = sum([1 for char in text if char in string.ascii_uppercase])
    return upc_no

def from_domain(text):
    '''separates domains FROM which the emails were sent from'''
    spt = text.split('\n') #splitting the text by \n
    try:
        headerlen=0
        for i in range(len(spt)):
            if "X-FileName:" in spt[i]: #headers tend to end on 'X-FileName:'
                headerlen=i + 1 #finding the header length
                break

        from_ = ""
        for i in range(headerlen):
            if "From:" in spt[i]: #finds the line with the information about the sender
                from_ = (str.split((spt[i][5:]))) #removes the actual word "From:" from the line
                #to = [x[:-1] for x in to]
                break

        from_d = []
        for e in from_:
            for i in range(len(e)):
                if e[i] == '@':
                    from_d.append(e[i+1:])
                    break
    except Exception as err:
        print(err)
    return from_d



def to_domain(text):
    '''separates domains TO which the emails were sent from'''
    spt = text.split('\n') #splitting the text by \n
    try:
        headerlen=0
        for i in range(len(spt)):
            if "X-FileName:" in spt[i]: #headers tend to end on 'X-FileName:'
                headerlen=i + 1 #finding the header length
                break
        to_ = ""
        for i in range(headerlen):
            if "To:" in spt[i]: #finds the line with the information about the recepient
                #print(spt[i])
                to_ = ([str.split((spt[i][3:]))]) #removes the actual word "To:" from the line
                for k in range(1,headerlen - i):
                    if "Subject" not in spt[i+k]:
                        to_.append(str.split((spt[i+k])))
                    else:
                        break
                break
        #print((to_))
        to_ = [item for sublist in to_ for item in sublist]
        #print((to_))
        
        if len(to_) > 1:
            to_ = [item[:-1] for item in to_ if "," in item ] #removes commas 
        #print((to_))
        to_d = []
        for e in to_:
            for i in range(len(e)):
                if e[i] == '@':
                    to_d.append(e[i+1:])
                    break
    except Exception as err:
        print(err)
    return to_d

def subject(text):
    '''separates the SUBJECT of the email'''
    spt = text.split('\n') #splitting the text by \n
    try:
        headerlen=0
        for i in range(len(spt)):
            if "X-FileName:" in spt[i]: #headers tend to end on 'X-FileName:'
                headerlen=i + 1 #finding the header length
                break

        subj_ = ""
        for i in range(headerlen):
            if "Subject:" in spt[i]: #finds the line with the subjrct field
                subj_ = (spt[i][8:]) #removes the actual word "Subject:" from the line
                break
    except Exception as err:
        print(err)
    return subj_
    
    
def date_time(text):
    '''separates the DATE of the email'''
    spt = text.split('\n') #splitting the text by \n
    try:
        headerlen=0
        for i in range(len(spt)):
            if "X-FileName:" in spt[i]: #headers tend to end on 'X-FileName:'
                headerlen=i + 1 #finding the header length
                break

        date = ""
        for i in range(headerlen):
            if "Date:" in spt[i]: #finds the line with the date field
                date = (spt[i][6:-12]) #removes the actual word "Date:\s" from the line
                #also apparently the dt.strptime really doesn't like the time zone in the 
                #format presented here
                date = dt.strptime(date, '%a, %d %b %Y %H:%M:%S')
                break
    except Exception as err:
        print(err)
    return date



def time_zone(text):
    '''separates the TIME ZONE of the email. Needed separetelly as datetime really doesnt like
    the format of the date imputted '''
    spt = text.split('\n') #splitting the text by \n
    try:
        headerlen=0
        for i in range(len(spt)):
            if "X-FileName:" in spt[i]: #headers tend to end on 'X-FileName:'
                headerlen=i + 1 #finding the header length
                break

        zone = ""
        for i in range(headerlen):
            if "Date:" in spt[i]: #finds the line with the date field
                zone = (spt[i][-4:-1]) #removes the 3 letter abbreviation of the timezone
                break
                
        if zone == 'PST':
            zone = 1
        else:
            zone = 0
    except Exception as err:
        print(err)
    return zone
    
def clean_text(text):
    '''returns clean text without punctuation, spaces and stopwords. TEXT IS NOT BROKEN DOWN INTO WORDS.
    usefull for n-grams. 
    
    recommended use only on short passages of text'''
    text = "".join([word.lower() for word in text if word not in string.punctuation])
    tokens = re.split('\W+', text)
    text = " ".join([wn.lemmatize(word) for word in tokens if word not in stopwords])
    return text

def clean_words(text):
    '''returns words separated from text, cleaning first for punctuation, spaces and stopwords.'''
    text = "".join([word.lower() for word in text if word not in string.punctuation])
    tokens = re.split('\W+', text)
    text = [ps.stem(word) for word in tokens if word not in stopwords]
    return text    


def features(pandasdataframe):
    '''function that extracts all features from text.
    
    IMPORTANT! 
    the pandasdataframe must be a pd obj and have the text of the emails in the "text" column'''
    #plain emails stats
    pandasdataframe['char_num'] = pandasdataframe['text'].apply(lambda x: char_calc(x))
    pandasdataframe['punct_num'] = pandasdataframe['text'].apply(lambda x: punct_count(x))
    pandasdataframe['punct_%'] = pandasdataframe['punct_num']/pandasdataframe['char_num']*100
    pandasdataframe['digits_num'] = pandasdataframe['text'].apply(lambda x: digits_count(x))
    pandasdataframe['digits_%'] = pandasdataframe['digits_num']/pandasdataframe['char_num']*100
    pandasdataframe['upcase_num'] = pandasdataframe['text'].apply(lambda x: upcase_count(x))
    pandasdataframe['upcase_%'] = pandasdataframe['upcase_num']/pandasdataframe['char_num']*100
    
    
    #domains stats
    pandasdataframe['domain_orig'] = pandasdataframe['text'].apply(lambda x: from_domain(x))
    pandasdataframe['domain_sent'] = pandasdataframe['text'].apply(lambda x: to_domain(x))
    
    
    #subject 
    pandasdataframe['subject'] = pandasdataframe['text'].apply(lambda x: subject(x))
    #subject stats
    pandasdataframe['subj_char_num'] = pandasdataframe['subject'].apply(lambda x: char_calc(x))
    pandasdataframe['subj_punct_num'] = pandasdataframe['subject'].apply(lambda x: punct_count(x))
    pandasdataframe['subj_punct_%'] = pandasdataframe['subj_punct_num']/pandasdataframe['subj_char_num']*100
    pandasdataframe['subj_digits_num'] = pandasdataframe['subject'].apply(lambda x: digits_count(x))
    pandasdataframe['subj_digits_%'] = pandasdataframe['subj_digits_num']/pandasdataframe['subj_char_num']*100
    pandasdataframe['subj_upcase_num'] = pandasdataframe['subject'].apply(lambda x: upcase_count(x))
    pandasdataframe['subj_upcase_%'] = pandasdataframe['subj_upcase_num']/pandasdataframe['subj_char_num']*100
    #subject cleaned and left as text - can be used for n-grams
    pandasdataframe['subj_clean_text'] = pandasdataframe['subject'].apply(lambda x: clean_text(x))
    

    #datetime of emails
    pandasdataframe['date_time'] = pandasdataframe['text'].apply(lambda x: date_time(x))
    pandasdataframe['time_zone'] = pandasdataframe['text'].apply(lambda x: time_zone(x))
    

    return pandasdataframe

def features_drop(pandasdataframe):
    #dropping everything that we don't need/ contains non-number values
    column_names = pandasdataframe.columns
    dropping_columns = ['text', 'category', 'domain_orig', 'domain_sent',
                        'subject',  'subj_clean_text','date_time', 'index']
    for i in dropping_columns:
        if i in column_names:
            pandasdataframe = pandasdataframe.drop([i], axis =1)
    return pandasdataframe

#emails_df = features(emails_df)
#
#emails_df = emails_df.fillna(value = 0)
#
#emails_df['sender_domain_len'] = emails_df['domain_orig'].apply(lambda x: char_calc(x[:]))
#for i in range(len(emails_df['sender_domain_len'])):
#    if emails_df['sender_domain_len'][i] == 0:
#        #print(i)
#        emails_df = emails_df.drop([i])
#
#emails_df = emails_df.reset_index()

from sklearn.externals import joblib
domain_to_model = joblib.load('domain_to_model.sav')
domain_from_model = joblib.load('domain_from_model.sav')
subj_model = joblib.load('subj_model.sav')
rf = joblib.load('rf.sav')

#from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
#
##to
#count_vect_to = CountVectorizer(analyzer = to_domain)
#domain_to_model = count_vect_to.fit(emails_df['text'])
#domain_to_counts = domain_to_model.transform(emails_df['text'])
#domain_to_counts_df = pd.DataFrame(domain_to_counts.toarray())
#domain_to_counts_df.columns = count_vect_to.get_feature_names()
#
##from
#count_vect_from = CountVectorizer(analyzer = from_domain)
#domain_from_model = count_vect_from.fit(emails_df['text'])
#domain_from_counts = domain_from_model.transform(emails_df['text'])
#domain_from_counts_df = pd.DataFrame(domain_from_counts.toarray())
#domain_from_counts_df.columns = count_vect_from.get_feature_names()
#
#
##SUBJECT data
#
#subj_ngram_vec = TfidfVectorizer(ngram_range =(1,2))
#subj_model = subj_ngram_vec.fit(emails_df['subj_clean_text'])
#subj_counts = subj_model.transform(emails_df['subj_clean_text'])
#subj_counts_df = pd.DataFrame(subj_counts.toarray())
#subj_counts_df.columns = subj_ngram_vec.get_feature_names()
#
#
#
#
#
#
##creating a pandas obj with features for machine learning
#emails_features = pd.concat([features_drop(emails_df),
#                            domain_to_counts_df,
#                            domain_from_counts_df,
#                            subj_counts_df],
#                           axis = 1)
#
##as we saw before some values are absent due to ddivision by zero
##just filling those with zeros, since if the subject doesn't have
##any charachters in it, then the % of punctuation is subsequently
##zero
#emails_features = emails_features.fillna(value = 0)
##print(emails_features.shape)
#
#
#
#from sklearn.ensemble import RandomForestClassifier
#from sklearn.model_selection import train_test_split
#
##our categories ended up being 'objects', not 'int' due to the way I set up the pd.dataframe above
#emails_df['category'] = emails_df['category'].astype('int')
#X_train, X_test, y_train, y_test = train_test_split(emails_features, emails_df['category'], test_size=0.33)
#
#
#
#from sklearn import metrics
#
##fitting the model
#
#rf = RandomForestClassifier(n_estimators=100, max_depth= None, n_jobs =1)
#rf_model = rf.fit(X_train, y_train)
#
#
##getting predictions
#y_pred = rf_model.predict(X_test)
#
##precision, recall and accuracy
#print( 'Accuracy: {}'.format(np.mean((y_pred==y_test))))
#print(metrics.classification_report(y_test, y_pred))
#
#
#
#
#from sklearn.externals import joblib
#filename = 'domain_to_model.sav'
#joblib.dump(domain_to_model, filename)
#
#
#filename_df = 'domain_from_model.sav'
#joblib.dump(domain_from_model, filename_df)
#
#filename_sm = 'subj_model.sav'
#joblib.dump(subj_model, filename_sm)
#
#
#filename_rf = 'rf.sav'
#joblib.dump(rf_model, filename_rf)


    #In[ ]:

# #same thing really but with pickle
#import pickle
#filename = 'domain_to_modelp.sav'
#pickle.dump(domain_to_model, open(filename, 'wb'))
#
#
#filename_df = 'domain_from_modelp.sav'
#pickle.dump(domain_from_model, open(filename_df, 'wb'))
#
#filename_sm = 'subj_modelp.sav'
#pickle.dump(subj_model, open(filename_sm, 'wb'))
#
#
#filename_rf = 'rfp.sav'
#pickle.dump(rf, open(filename_rf, 'wb'))
#

# In[27]:
#importing the text file
import argparse


parser = argparse.ArgumentParser()

parser.add_argument("email", help="The email file you want classified", type=str)

args = parser.parse_args()

email_file = args.email

#email_file = input("Enter here your path/to/email: ")
print("This classifier is trained on Enron data, so to classify other company's emails please enter"
      " a valid domain for that company (without @, but with top-level domain (eg. 'enron.com')."
      " If you wish to proceed otherwise, type SKIP below.")
company_domain = input("Enter your company domain here: ")

punct = string.punctuation
if '.' in punct:
    dot_indedx = punct.index('.')
    punct = ''.join((punct[0:(dot_indedx)],(punct[dot_indedx+1::])))
    dot_indedx = punct.index('@')
    punct = ''.join((punct[0:(dot_indedx)],(punct[dot_indedx+1::])))

def domain_format_checker(domain):
    '''checks that the manually input domain is in correct format'''
    domain = "".join(re.split('\s', domain))
    correct = True
    if "." not in domain:
        print("Error: incorrect format, no dots found")
        correct = False
    elif "@" in domain:
        at_indedx = domain.index('@')
        domain = ''.join((domain[0:(at_indedx)],(domain[at_indedx+1::])))
    for i in punct:
        if i in domain:
            print("Error: incorrect charachter encountered")
            correct = False
    return correct, domain.lower()

def domain_replacer(pdf, domain):
    '''Finds the given DOMAIN in the PANDASDATAFRAME and replaces its instance both in the sender and
        the reciever field with enron.com. Done to mask the reliance on specifically on enron data.'''
    colmns = ['domain_orig', 'domain_sent']
    for i in colmns:
        if i in pdf:
            for j in range(pdf.shape[0]):
                for k in range(len(pdf[i].iloc[j])):
                    if (pdf[i].iloc[j])[k] == domain:
                        pdf[i].iloc[j][k] = 'enron.com'
    return pdf

disclamer = "Disclamer! If you don't provide a domain, classification may be innacurate!"
if company_domain.lower() == 'skip':#accepting all variations of skip
    print(disclamer)
else:
    checked = domain_format_checker(company_domain)
    while checked[0] == False:
        if company_domain.lower() == 'skip':
            print(disclamer)
            break
        company_domain = input('Re-enter your domain here: ')
        checked = domain_format_checker(company_domain)



with open(email_file, 'r', encoding = 'windows-1252') as f:
    email_text = f.read()


# In[ ]:
try:
    text_df = pd.DataFrame([email_text], columns = ['text'])
    text_features_df = features(text_df)


    text_features_df['sender_domain_len'] = text_features_df['domain_orig'].apply(lambda x: char_calc(x[:]))
    for i in (text_features_df['sender_domain_len']):
        if i == 0:
            text_features_df = text_features_df.drop([i])
            print('No sender domain found, email cannot be categorised')
        else:
            continue

    text_features_df = text_features_df.reset_index()

    if company_domain.lower() != 'skip':
        text_features_df = domain_replacer(text_features_df, checked[1])
    #print(text_features_df.iloc[0,9:11])

    domain_to_counts = domain_to_model.transform(text_features_df['text'])
    domain_to_counts_df = pd.DataFrame(domain_to_counts.toarray())

    domain_from_counts = domain_from_model.transform(text_features_df['text'])
    domain_from_counts_df = pd.DataFrame(domain_from_counts.toarray())

    subj_counts = subj_model.transform(text_features_df['subj_clean_text'])
    subj_counts_df = pd.DataFrame(subj_counts.toarray())


    features_text = features_drop(text_features_df)

    text_features_droppedd_df = pd.concat([features_text,
                                            domain_to_counts_df,
                                            domain_from_counts_df,
                                            subj_counts_df],
                                           axis = 1)
    text_features_droppedd_df = text_features_droppedd_df.fillna(value = 0)

    prediction =  rf.predict(text_features_droppedd_df)
    label = list((rf).classes_.astype('str'))
    pred_prob = rf.predict_proba(text_features_droppedd_df)*100
    pred_prob_df = pd.DataFrame(pred_prob, columns = label)
    
    for i in range(len(prediction)):
        if prediction[i] == 1:
            print(f'{pred_prob_df.iloc[i,1]}% probability this is a business email')
        elif prediction[i] == 0:
            print(f'{pred_prob_df.iloc[i,0]}% probability this is a personal email')
        else:
            print('oh-oh')
except:
    print('Error: cannot categorise')



